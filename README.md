# docker-deploy
Small utility to run Docker commands remotely (for example from CI pipeline).

**For internal use only.**

# Usage
To start docker-deploy on port 8080:

```bash
docker pull registry.gitlab.com/Xatom/docker-deploy
docker stop docker-deploy
docker rm docker-deploy
docker run -dit -e DOCKER_DEPLOY_API_KEY=[random key] -p 8080:80 \
    --name docker-deploy \
    --restart unless-stopped \
    -v /var/run/docker.sock:/var/run/docker.sock \
    registry.gitlab.com/Xatom/docker-deploy:latest
```

Make sure to replace `[random key]` with a random key.

If you are running docker-deploy behind a reverse proxy, please add `-e DOCKER_DEPLOY_USE_FORWARDED_IP=true`.

You can add trusted IPs by adding `-e DOCKER_DEPLOY_TRUSTED_IPS="1.2.3.4 5.6.7.8"` (delimited by spaces).

# API Usage
You can call the API using cURL:

```bash
curl -sfX POST -F "api_key=$API_KEY" -F "args=pull $DOCKER_IMAGE" "http://$API_HOST/deploy"
```

In this example, the API is called at `$API_HOST` using API key `$API_KEY`, executing `docker pull $DOCKER_IMAGE` remotely.
Output is sent back as plain text and the `-f` flag propagates fails to the `curl` command itself.

Possible HTTP status codes:
* `200`: the command completed successfully
* `400`: something is wrong with the request (see response)
* `500`: something is wrong with the command (see response)
