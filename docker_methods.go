package main

import (
	"bytes"
	"os/exec"
)

const (
	DockerCommand = "docker"
)

func runCommand(cmd string, args []string) (err error, output []byte) {
	if cmd == "" {
		cmd = DockerCommand
	}

	command := exec.Command(cmd, args...)
	command.Dir = baseDir

	var out bytes.Buffer
	command.Stdout = &out
	command.Stderr = &out

	return command.Run(), out.Bytes()
}
