FROM golang:alpine AS build

WORKDIR /go/src/docker-deploy
COPY . .

RUN go get -v
RUN go build -v

WORKDIR /

ENV DOCKER_VERSION=18.03.0-ce

RUN apk add --no-cache curl
RUN curl -fsSL https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz -o docker.tgz
RUN tar xzvf docker.tgz

FROM alpine:latest

WORKDIR /app
COPY --from=build /go/bin/docker-deploy .
COPY --from=build /docker/docker /usr/local/bin

RUN apk add --no-cache bash

ARG version=Test

ENV DOCKER_DEPLOY_VERSION=${version}
ENV DOCKER_DEPLOY_USE_FORWARDED_IP=false

EXPOSE 80

ENTRYPOINT ["./docker-deploy"]
