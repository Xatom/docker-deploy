package main

import (
	"log"
	"net/http"
	"strings"
	"fmt"
	"net"
)

const (
	LogFormat              = "Request %s from %s to %s [%s]"
	ExecutingCommandFormat = "Executing %s in %s with args %s"

	IncomingRequest  = "Incoming request"
	InvalidMethod    = "API method doesn't exist"
	InvalidIpAddress = "Invalid IP address"
	InvalidApiKey    = "Invalid API key"
	InvalidCommand   = "Command resulted in error %v"

	CmdName          = "cmd"
	ArgsName         = "args"
	ApiKeyName       = "api_key"
	ForwardedForName = "X-Forwarded-For"

	ArgsSeparator         = " "
	ForwardedForSeparator = ", "
)

func realRemoteIp(request *http.Request) string {
	if !useForwardedIp {
		return *getIp(request.RemoteAddr)
	}

	forwardedIpHeader := request.Header.Get(ForwardedForName)
	forwardedIpString := strings.Split(forwardedIpHeader, ForwardedForSeparator)[0]
	forwardedIp := getIp(forwardedIpString)

	if forwardedIp == nil {
		return *getIp(request.RemoteAddr)
	}

	return *forwardedIp
}

func getIp(hostPort string) *string {
	host, _, err := net.SplitHostPort(hostPort)
	if err == nil {
		return &host
	}

	ip := net.ParseIP(hostPort)
	if ip == nil {
		return nil
	}

	host = ip.String()
	return &host
}

func logRequest(request *http.Request, message string) {
	log.Printf(LogFormat, request.Method, realRemoteIp(request), request.URL, message)
}

func writeError(request *http.Request, response http.ResponseWriter, message string, status int) {
	logRequest(request, message)

	response.WriteHeader(status)
	response.Write([]byte(message + "\n"))
}

func handleNotFound(response http.ResponseWriter, request *http.Request) {
	logRequest(request, IncomingRequest)

	writeError(request, response, InvalidMethod, http.StatusNotFound)
}

func handleDeploy(response http.ResponseWriter, request *http.Request) {
	if request.Method != http.MethodPost {
		handleNotFound(response, request)
		return
	}

	logRequest(request, IncomingRequest)

	if !validIp(response, request) {
		return
	}

	if !validApiKey(response, request) {
		return
	}

	cmd := request.FormValue(CmdName)
	args := request.FormValue(ArgsName)

	logRequest(request, fmt.Sprintf(ExecutingCommandFormat, cmd, baseDir, args))

	err, out := runCommand(cmd, strings.Split(args, ArgsSeparator))
	if err != nil {
		writeError(request, response, fmt.Sprintf(InvalidCommand, err), http.StatusInternalServerError)
	}

	response.Write(out)
}

func validIp(response http.ResponseWriter, request *http.Request) bool {
	if len(trustedIps) == 0 {
		return true
	}

	for i := 0; i < len(trustedIps); i++ {
		if realRemoteIp(request) == trustedIps[i] {
			return true
		}
	}

	writeError(request, response, InvalidIpAddress, http.StatusBadRequest)
	return false
}

func validApiKey(response http.ResponseWriter, request *http.Request) bool {
	requestApiKey := request.FormValue(ApiKeyName)

	if requestApiKey == apiKey {
		return true
	}

	writeError(request, response, InvalidApiKey, http.StatusBadRequest)
	return false
}
