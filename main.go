package main

import (
	"log"
	"net/http"
	"os"
	"strings"
	"strconv"
	"net"
)

const (
	DefaultVersion = "Development"

	VersionLogFormat      = "Running docker-deploy @ %s"
	InvalidEnvValueFormat = "Invalid value for '%s': %v"

	DefaultPath = "/"
	DeployPath  = "/deploy"

	VersionEnvName        = "DOCKER_DEPLOY_VERSION"
	ApiKeyEnvName         = "DOCKER_DEPLOY_API_KEY"
	BaseDirEnvName        = "DOCKER_DEPLOY_BASE_DIR"
	TrustedIpsEnvName     = "DOCKER_DEPLOY_TRUSTED_IPS"
	UseForwardedIpEnvName = "DOCKER_DEPLOY_USE_FORWARDED_IP"

	TrustedIpsSeparator = ' '
)

var (
	version        = getVersion()
	apiKey         = getApiKey()
	baseDir        = getBaseDir()
	trustedIps     = getTrustedIps()
	useForwardedIp = getUseForwardedIp()
)

func main() {
	log.Printf(VersionLogFormat, version)

	http.HandleFunc(DefaultPath, handleNotFound)
	http.HandleFunc(DeployPath, handleDeploy)

	err := http.ListenAndServe(":80", nil)
	if err != nil {
		log.Fatal(err)
	}
}

func getVersion() string {
	versionEnv := os.Getenv(VersionEnvName)
	if versionEnv == "" {
		versionEnv = DefaultVersion
	}

	return versionEnv
}

func getApiKey() string {
	return os.Getenv(ApiKeyEnvName)
}

func getBaseDir() string {
	return os.Getenv(BaseDirEnvName)
}

func getTrustedIps() []string {
	trustedIpsEnv := os.Getenv(TrustedIpsEnvName)

	ips := strings.FieldsFunc(trustedIpsEnv, func(character rune) bool {
		return character == TrustedIpsSeparator
	})

	for i := 0; i < len(ips); i++ {
		ip := net.ParseIP(ips[i])
		if ip == nil {
			log.Fatalf(InvalidEnvValueFormat, TrustedIpsEnvName, nil)
		}
	}

	return ips
}

func getUseForwardedIp() bool {
	useForwardedIpEnv := os.Getenv(UseForwardedIpEnvName)

	val, err := strconv.ParseBool(useForwardedIpEnv)
	if err != nil {
		log.Fatalf(InvalidEnvValueFormat, UseForwardedIpEnvName, err)
	}

	return val
}
